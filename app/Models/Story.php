<?php

namespace App\Models;

use App\Enums\StoryStatusEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Story extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $attributes = [
        'active' => true,
        'status' => StoryStatusEnum::MODERATION->value,
    ];

    protected $fillable = [
        'title',
        'text',
        'active',
        'status',
    ];

    protected $casts = [
        'active' => 'boolean',
        'status' => StoryStatusEnum::class,
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
