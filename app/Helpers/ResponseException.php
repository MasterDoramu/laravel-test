<?php
namespace App\Helpers;
use Illuminate\Http\Exceptions\HttpResponseException;

final class ResponseException {
    public function __construct($message, $code = 400)
    {
        throw new HttpResponseException(
            response()->json(
                [
                    'message' => $message,
                    'code' => $code,
                ],
                $code,
                [ 'Content-Type' => 'application/json; charset=utf-8' ],
                JSON_UNESCAPED_UNICODE
            )
        );
    }
}
