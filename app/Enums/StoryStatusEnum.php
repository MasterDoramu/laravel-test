<?php

namespace App\Enums;

enum StoryStatusEnum: string
{
    case MODERATION = 'moderation';
    case PUBLISHED = 'published';
    case REJECTED = 'rejected';
}
