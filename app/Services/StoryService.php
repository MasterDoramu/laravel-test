<?php

namespace App\Services;

use App\Dto\Story\CreateStoryDto;
use App\Dto\Story\ModerateStoryDto;
use App\Enums\StoryStatusEnum;
use App\Models\Story;
use App\Models\User;

final class StoryService
{
    public function findAllPublish()
    {
        return Story::where('status', StoryStatusEnum::PUBLISHED->value)
            ->where('active', true)
            ->get();
    }

    public function findAll(User $user)
    {
        if (! $user->admin) {
            return $user->stories()->get();
        }

        return Story::all();
    }

    public function findAllModeration()
    {
        return Story::where(
            'status',
            StoryStatusEnum::MODERATION->value
        )->get();
    }

    public function moderate(int $id, ModerateStoryDto $moderateStoryDto)
    {
        $story = $this->findById($id);
        $story->status = $moderateStoryDto->status;
        $story->saveOrFail();
    }

    public function update(CreateStoryDto $dto, Story $story)
    {
        $story->title = $dto->title;
        $story->text = $dto->text;
        $story->active = $dto->active;
        $story->saveOrFail();
    }

    public function create(CreateStoryDto $dto, User $user)
    {
        $story = new Story();
        $story->title = $dto->title;
        $story->text = $dto->text;
        $story->active = $dto->active;
        $story->user_id = $user->id;
        $story->saveOrFail();
    }

    public function delete(Story $story)
    {
        $story->deleteOrFail();
    }

    public function findById(int $id)
    {
        return Story::findOrFail($id);
    }
}
