<?php

namespace App\Services;

use App\Dto\Auth\LoginDto;
use App\Dto\Auth\RegisterDto;
use App\Helpers\ResponseException;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

final class UserService
{
    public function register(RegisterDto $registerDto)
    {
        try {
            $this->findOneByEmail($registerDto->email->value());
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $user = new User([
                'email' => $registerDto->email->value(),
                'name' => $registerDto->name,
                'password' => Hash::make($registerDto->password),
            ]);

            if ($user->save()) {
                return $this->generateResponseWithToken($user);
            }

            new ResponseException('Ой, что-то пошло не так');
        }

        new ResponseException('Пользователь с такой почтой уже существует');
    }

    public function login(LoginDto $loginDto)
    {
        if (! Auth::attempt([
            'email' => $loginDto->email->value(),
            'password' => $loginDto->password,
        ])) {
            new ResponseException('Пользователь не найден');
        }

        $user = User::where('email', $loginDto->email->value())->firstOrFail();

        return $this->generateResponseWithToken($user);
    }

    public function logout(User $user)
    {
        $user->tokens()->delete();
        new ResponseException('Вы успешно разлогинились', 200);
    }

    private function generateResponseWithToken(User $user)
    {
        $token = $user->createToken('auth_token')->plainTextToken;

        return [
            'data' => $user,
            'access_token' => $token,
            'token_type' => 'Bearer',
        ];
    }

    private function findOneByEmail(string $email): User
    {
        return User::where('email', $email)->firstOrFail();
    }
}
