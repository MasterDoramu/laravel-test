<?php

namespace App\Http\Controllers;

use App\Dto\Auth\Email;
use App\Dto\Auth\LoginDto;
use App\Services\UserService;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(Request $request, UserService $userService)
    {
        $validated = $request->validate([
            'name' => ['required', 'string', 'min:3', 'max:100'],
            'email' => ['required', 'string', 'email', 'max:100'],
            'password' => ['required', 'string', 'min:8'],
        ]);
        $dto = new \App\Dto\Auth\RegisterDto(
            $validated['name'],
            Email::create($validated['email']),
            $validated['password']
        );

        return $userService->register($dto);
    }

    public function login(Request $request, UserService $userService)
    {
        $validated = $request->validate([
            'email' => ['required', 'string', 'email', 'max:100'],
            'password' => ['required', 'string', 'min:8'],
        ]);
        $dto = new LoginDto(Email::create(
            $validated['email']),
            $validated['password']
        );

        return $userService->login($dto);
    }

    public function logout(UserService $userService, Request $request)
    {
        $userService->logout($request->user());
    }

    public function profile(Request $request)
    {
        return $request->user();
    }
}
