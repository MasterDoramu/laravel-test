<?php

namespace App\Http\Controllers;

use App\Dto\Story\CreateStoryDto;
use App\Dto\Story\ModerateStoryDto;
use App\Enums\StoryStatusEnum;
use App\Models\Story;
use App\Services\StoryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoryController extends Controller
{
    public function findAllPublish(StoryService $storyService)
    {
        return $storyService->findAllPublish();
    }

    public function findAll(StoryService $storyService, Request $request)
    {
        return $storyService->findAll($request->user());
    }

    public function findAllModeration(StoryService $storyService)
    {
        Gate::authorize('admin', Story::class);

        return $storyService->findAllModeration();
    }

    public function update(StoryService $storyService, Request $request, $id)
    {
        $story = $storyService->findById($id);
        Gate::authorize('accessToModel', $story);

        $validated = $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'text' => ['required', 'string'],
            'active' => ['boolean'],
        ]);
        $dto = new CreateStoryDto(
            $validated['title'],
            $validated['text'],
            array_key_exists('active', $validated)
            ? $validated['active']
            : true,
        );

        $storyService->update($dto, $story);
    }

    public function create(StoryService $storyService, Request $request)
    {
        $validated = $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'text' => ['required', 'string'],
            'active' => ['boolean'],
        ]);
        $dto = new CreateStoryDto(
            $validated['title'],
            $validated['text'],
            array_key_exists('active', $validated)
            ? $validated['active']
            : true,
        );

        $storyService->create($dto, $request->user());
    }

    public function delete(StoryService $storyService, $id)
    {
        $story = $storyService->findById($id);
        Gate::authorize('accessToModel', $story);

        $storyService->delete($story);
    }

    public function moderate(StoryService $storyService, Request $request, $id)
    {
        Gate::authorize('admin', Story::class);

        $validated = $request->validate([
            'status' => [Rule::enum(StoryStatusEnum::class)],
        ]);
        $dto = new ModerateStoryDto($validated['status']);

        return $storyService->moderate((int) $id, $dto);
    }
}
