<?php

namespace App\Policies;

use App\Helpers\ResponseException;
use App\Models\Story;
use App\Models\User;
use Illuminate\Support\Facades\Gate;

class StoryPolicy
{
    /**
     * Create a new policy instance.
     */
    public function admin()
    {
        Gate::allows('admin');

        return true;
    }

    public function accessToModel(User $user, Story $story)
    {
        if ($user->isAdmin() || $user->id === $story->user_id) {
            return true;
        }

        new ResponseException('Нет доступа к обновлению', 403);
    }
}
