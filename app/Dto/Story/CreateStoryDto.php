<?php

namespace App\Dto\Story;

final readonly class CreateStoryDto
{
    public function __construct(
        public string $title,
        public string $text,
        public bool $active,
    ) {
    }
}
