<?php

namespace App\Dto\Story;

final readonly class ModerateStoryDto
{
    public function __construct(
        public string $status
    ) {
    }
}
