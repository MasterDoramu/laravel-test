<?php
namespace App\Dto\Auth;

use App\Dto\Auth\Email;

readonly final class RegisterDto
{
    public function __construct(
        public string $name,
        public Email $email,
        public string $password,
    ) {}
}
