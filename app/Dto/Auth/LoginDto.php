<?php
namespace App\Dto\Auth;

use App\Dto\Auth\Email;

readonly final class LoginDto
{
    public function __construct(
        public Email $email,
        public string $password,
    ) {}
}
