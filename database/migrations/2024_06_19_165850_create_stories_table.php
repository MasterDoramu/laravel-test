<?php

use App\Enums\StoryStatusEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('stories', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();

            $table->foreignId('user_id')->constrained('users');

            $table->string('title');
            $table->boolean('active')->default(true);
            $table->text('text');
            $table->enum(
                'status',
                array_column(StoryStatusEnum::cases(), 'value')
            )->default(StoryStatusEnum::MODERATION->value);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('stories');
    }
};
