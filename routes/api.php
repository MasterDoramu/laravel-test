<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\StoryController;
use Illuminate\Support\Facades\Route;

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
Route::get('/story/publish', [StoryController::class, 'findAllPublish']);

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/logout', [AuthController::class, 'logout']);
    Route::get('/profile', [AuthController::class, 'profile']);

    Route::prefix('/story')->group(function () {
        Route::get('', [StoryController::class, 'findAll']);
        Route::put('{id}', [StoryController::class, 'update']);
        Route::post('', [StoryController::class, 'create']);
        Route::delete('{id}', [StoryController::class, 'delete']);

        Route::get('/moderation', [
            StoryController::class,
            'findAllModeration',
        ]);
        Route::put('moderation/{id}', [StoryController::class, 'moderate']);
    });
});
